package com.srrit.web.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private CustomAuthenticationProvider customAuthenticationProvider;

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {

		// Configures form login
		httpSecurity.formLogin().loginPage("/login").loginProcessingUrl("/login").
				defaultSuccessUrl("/login-success").failureUrl("/login?error")
		.usernameParameter("email").passwordParameter("password")
		// Configures the logout function
		.and().logout().deleteCookies("JSESSIONID").logoutUrl("/logout")
		// Configures url based authorization
		.and().authorizeRequests()
		// Anyone can access the urls
		.antMatchers("/", "/login", "/home", "/coming-soon", "/contact", "/terms",
				"/privacy","/about-us","/signup","/verifyAccount","/reset-password",
				"/changePassword","/updatePassword","/subscribe-email","/unsubscribeEmail","/unsubscribe")
				.permitAll()
		.antMatchers("/users").
				hasAnyAuthority("ROLE_ADMIN","ROLE_PROVIDER");
		httpSecurity.csrf().disable();
		httpSecurity.headers().frameOptions().disable();
		httpSecurity.sessionManagement().enableSessionUrlRewriting(false);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**", "/resources/static/***",
				"/resources/static/images/***","/webjars/**");

	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(this.customAuthenticationProvider);
	}

}