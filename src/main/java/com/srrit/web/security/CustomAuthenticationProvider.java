package com.srrit.web.security;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.srrit.web.common.util.HashGenerator;
import com.srrit.web.user.domain.User;
import com.srrit.web.user.service.UserService;
 
 
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
 
    @Autowired
    private UserService userService; 
    
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException{
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();
 
        User user = userService.loadUserByUsername(username.toLowerCase());
 
        if (user == null) {
            throw new UsernameNotFoundException("Username not found.");
        }else if (!(HashGenerator.encryptPassword(password.trim()).equalsIgnoreCase(user.getPassword()))) {
            throw new BadCredentialsException("Wrong password.");
         }else if(user.getIsVerified() == 0) {
        	 throw new AccountExpiredException("User is not verified.");
         }else if(user.getUserStatus() == 0) { 
        	 throw new DisabledException("User is disabled."); 
         } 
 
        Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
 
        return new UsernamePasswordAuthenticationToken(user, password, authorities);
    }
 
    @Override
    public boolean supports(Class<?> arg0) {
        return true;
    }
}
