package com.srrit.web.user.service;

import org.springframework.data.domain.Sort;

import com.srrit.web.user.domain.Donor;

public interface DonorService {
  
	Iterable<Donor> listAllDonors(Sort sort);

	Donor getDonorById(Integer id);

	Donor saveDonor(Donor provider);

	void deleteDonor(Integer id);

	Donor findDonorByUsername(String username);
	
	Donor findDonorByEmail(String email);
}
