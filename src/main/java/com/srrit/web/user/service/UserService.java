package com.srrit.web.user.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.srrit.web.user.domain.User;
 
@Service
public class UserService implements UserDetailsService { 

	@PersistenceContext
	private EntityManager em;
	  
	public Collection<GrantedAuthority> getAuthorities(User user) {

		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(2);

		if(user != null && user.getRole() != null) {
			authList.add(new SimpleGrantedAuthority(user.getRole().name()));
		}
		return authList;
	}

	public User loadUserByUsername(String username) throws UsernameNotFoundException {
 		User user = null;
		try {
 				TypedQuery<User> query = em.createQuery("select u from User u where u.username = :username", User.class);

				query.setParameter("username", username);
				user = query.getSingleResult();
 		} catch (NoResultException nre) {
			return null;
		}
		return user;
	}  
	
	public User loadUserByEmail(String email) throws UsernameNotFoundException {
 		User user = null;
		try {
 				TypedQuery<User> query = em.createQuery("select u from User u where u.email = :email", User.class);

				query.setParameter("email", email);
				user = query.getSingleResult();
 		} catch (NoResultException nre) {
			return null;
		}
		return user;
	}  
}