package com.srrit.web.user.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.srrit.web.user.domain.Donor;
import com.srrit.web.user.repository.DonorRepository;

@Service
public class DonorServiceImpl implements DonorService {

	private static final Log logger = LogFactory.getLog(DonorServiceImpl.class);
 	 
	@Autowired
	private DonorRepository donorRepository; 
	  
	@PersistenceContext
	private EntityManager em; 
	 
	@Override
	public Iterable<Donor> listAllDonors(Sort sort) {
		return donorRepository.findAll(sort);
	}

	@Override
	public Donor getDonorById(Integer id) {
		return donorRepository.findOne(id);
	}

	@Override
	public Donor saveDonor(Donor donor) {
		return donorRepository.save(donor);

	}

	@Override
	public void deleteDonor(Integer id) {
		donorRepository.delete(id);
	}

	@Override
	public Donor findDonorByUsername(String username) {

		Donor donor = null;
		
		try { 
		  
			TypedQuery<Donor> query = em.createQuery(
					"select d from Donor d left join fetch d.user u where u.userStatus = 1 "
					+ "and u.username = :username", Donor.class);

			query.setParameter("username", username);
			donor = query.getSingleResult();

		} catch (Exception nre) {
			logger.error("Exception occured : findDonorByUsername() - Donor Not Found >>>> " + username);
		}
		return donor;
	}
	
	@Override
	public Donor findDonorByEmail(String email) {

		Donor donor = null;
		
		try { 
		  
			TypedQuery<Donor> query = em.createQuery(
					"select d from Donor d left join fetch d.user u where u.userStatus = 1 "
					+ "and u.email = :email", Donor.class);

			query.setParameter("email", email);
			donor = query.getSingleResult();

		} catch (Exception nre) {
			logger.error("Exception occured : findDonorByEmail() - Donor Not Found >>>> " + email);
		}
		return donor;
	}
}
