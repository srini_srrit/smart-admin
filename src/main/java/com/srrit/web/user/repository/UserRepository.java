package com.srrit.web.user.repository;
 
import org.springframework.data.repository.PagingAndSortingRepository;

import com.srrit.web.user.domain.User;
 
public interface UserRepository extends PagingAndSortingRepository<User, Long>{
	 
    User findByEmail(String email);    
}
