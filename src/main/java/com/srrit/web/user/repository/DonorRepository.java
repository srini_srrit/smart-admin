package com.srrit.web.user.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.srrit.web.user.domain.Donor;
 
public interface DonorRepository extends PagingAndSortingRepository<Donor, Integer>{
}
