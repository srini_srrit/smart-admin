package com.srrit.web.user.controller;
  
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult; 
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.srrit.web.app.constants.EmailConstants;
import com.srrit.web.app.constants.MessageConstants;
import com.srrit.web.common.bean.EmailBean;
import com.srrit.web.common.configuration.B4LMessages;
import com.srrit.web.common.controller.AbstractController;
import com.srrit.web.common.service.EmailService;
import com.srrit.web.common.service.EncryptionService;
import com.srrit.web.common.util.HashGenerator;
import com.srrit.web.user.domain.Donor;
import com.srrit.web.user.domain.Role;
import com.srrit.web.user.domain.User;
import com.srrit.web.user.domain.UserType;
import com.srrit.web.user.service.DonorService;
import com.srrit.web.user.service.UserService;
 
/**
 * 
 * @author Aruna Vemula
 *
 */
@Controller
public class DonorController extends AbstractController{

	public static final Log logger = LogFactory.getLog(DonorController.class);

	@Autowired
	private DonorService donorService;   
	 
	@Autowired
	private EmailService emailService;  

	@Autowired
	private UserService userService; 
 	
	@Value("${account.verifylink}")
	private String verifylink; 
	
	@Value("${app.url}")
    private String siteUrl;   
 	
	@Autowired
    private B4LMessages messages;
	
	@Value("${SENDER_USERNAME}")
    private String senderEmail;

	@RequestMapping(value = "donorsList", method = RequestMethod.GET)
	public String getAllDonorsList(Model model){

		Iterable<Donor> list = donorService.listAllDonors(sortByLastUpdatedDesc());
		model.addAttribute("donors", list);
		return "donorsList";
	}

	private Sort sortByLastUpdatedDesc() {
		return new Sort(Sort.Direction.DESC, "lastUpdated");
	}
	@RequestMapping(value="signup")
	public String newProvider(Model model){
		model.addAttribute("donor", new Donor());
		
		return "donor/register";
	}

	@RequestMapping(value = "signup", method = RequestMethod.POST)
	public String saveProvider(@Valid Donor donor, HttpServletRequest request, Model model, BindingResult errors){ 
		
		User existingUser = userService.loadUserByEmail(donor.getUser().getEmail()); 
		
	 	if(errors.hasErrors()) {  
	 		model.addAttribute("error", "Please input the all required fields.");
			model.addAttribute("donor", donor);  
			return "donor/register";  
			
		}if(null != existingUser) {
			model.addAttribute("error", messages.get(MessageConstants.EMAIL_REGISTERED));
			model.addAttribute("donor", donor); 
			model.addAttribute("isExisting", "true");
			return "donor/register"; 
		}else {  
			
			User user = donor.getUser();
			user.setUpdatedUser(user.getUsername());
			user.setUserType(UserType.P); 
			user.setRole(Role.ROLE_PROVIDER); 
			user.setUsername(user.getEmail());
			user.setUpdatedUser(user.getEmail());  
			user.setCreationTime(Calendar.getInstance().getTime());
			String pass = user.getPassword();
			user.setPassword(HashGenerator.encryptPassword(pass.trim()));  
		  	donor.setUser(user);
			donor.setMobile(donor.getPhone());
	 
			donorService.saveDonor(donor);   
	
			EmailBean email = new EmailBean();
			email.setDonor(donor); 
			email.setToEmail(user.getEmail());
			email.setSubject(EmailConstants.USER_WELCOME);  
			String encryptedUrl = null;
		 	encryptedUrl = EncryptionService.encodeStandard(user.getEmail());
		 	email.setAccountVerifyLink(verifylink + encryptedUrl); 
			email.setUser(user);
			emailService.sendEmail(email, EmailConstants.EMAILBODY_USER_WELCOME);
			model.addAttribute("mesg", messages.get(MessageConstants.SIGNUP_SUCCESS_MESG) +" <b>"+senderEmail+"</b>"); 
			return "success";
		}
	}   
	 
}
