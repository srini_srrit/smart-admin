
/**
 * 
 */
package com.srrit.web.user.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model; 
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam; 
import com.srrit.web.app.constants.AppConstants;
import com.srrit.web.app.constants.EmailConstants;
import com.srrit.web.app.constants.MessageConstants;
import com.srrit.web.common.bean.EmailBean;
import com.srrit.web.common.configuration.B4LMessages;
import com.srrit.web.common.controller.AbstractController;
import com.srrit.web.common.service.EmailService;
import com.srrit.web.common.service.EncryptionService;
import com.srrit.web.common.util.HashGenerator;
import com.srrit.web.common.util.Utility;
import com.srrit.web.user.domain.Donor;
import com.srrit.web.user.domain.User;
import com.srrit.web.user.repository.UserRepository;
import com.srrit.web.user.service.DonorService;
import com.srrit.web.user.service.UserService;

@Controller
public class UserController extends AbstractController{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);  
	
	@Autowired
    private B4LMessages messages; 
	
	@Autowired
    private UserRepository userRepository;
	
	@Autowired
	private EmailService emailService;  

	@Autowired
	private UserService userService;  
	
	@Autowired
	private DonorService donorService;   
	
	@Value("${account.resetpassword}")
    private String resetPasswordLink; 
	
	@Value("${account.verifylink}")
    private String verifylink; 
	
	@Value("${app.url}")
    private String siteUrl; 
	
	@Value("${account.passwordreseturl}")
    private String passwordResetUrl;  
	
	@Value("${SENDER_USERNAME}")
	private String senderEmail; 
		 
	   
	@RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(@RequestParam(value = "error", required = false) String error, 
    		HttpServletRequest request,HttpServletResponse response, Model model) {
		
		if (error != null) {
			model.addAttribute("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}
	 
		response.setStatus(HttpStatus.OK.value());

		//request.getSession().setAttribute("visits", getSiteVisits(request));
		
        return "user/login";
    }
	//customize the error message
	private String getErrorMessage(HttpServletRequest request, String key){
		
			Exception exception = (Exception) request.getSession().getAttribute(key); 
			String error = "";
			if(exception instanceof UsernameNotFoundException) {
				error = messages.get(MessageConstants.USER_NOT_FOUND);
			}else if (exception instanceof BadCredentialsException) {
				error = messages.get(MessageConstants.INVALID_USER);
			}else if (exception instanceof AccountExpiredException) {
				error = messages.get(MessageConstants.USER_NOT_VERIFIED);
			}else if (exception instanceof DisabledException) {
				error = messages.get(MessageConstants.USER_DISABLED);
			}else{	
				error = "Invalid username and/or password";
			} 
			return error;
	}
		
	@RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){    
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
       
        return "redirect:/login?logout";
    }
	
	@RequestMapping("/login-success")
    public String loginSuccess(HttpServletRequest request, HttpServletResponse response, Model model){
 	 	return "redirect:/";
 	 	
 	}
	
	@RequestMapping(value="/verifyAccount", method = RequestMethod.GET)
	public String verifyAccount(HttpServletRequest request,
			HttpServletResponse response, Model uiModel) { 
		
		String userName = request.getParameter("verifyUser");
		LOGGER.debug("Verify user name encoded :" + userName);
		userName = EncryptionService.decodeStandard(userName);
		LOGGER.debug("Verify user name :" + userName);
		
		User user = userService.loadUserByUsername(userName);

		if (user != null) {
			user.setIsVerified(AppConstants.ACTIVE_STATUS);
			user.setUserStatus(AppConstants.ACTIVE_STATUS);
			userRepository.save(user);
		}
		
		String return_url = request.getParameter("return_url");
		if(!StringUtils.isBlank(return_url) && EncryptionService.decodeStandard(return_url).contains("buy-tickets")) {
			//setting the authentication
			Authentication auth = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
		    SecurityContextHolder.getContext().setAuthentication(auth);
			return "forward:"+EncryptionService.decodeStandard(return_url); 
		}else {
			uiModel.addAttribute("resetMsg", MessageConstants.ACTIVATED_SUCCESS_MSG); 
			return "user/login";
		}
	}   
	
	@RequestMapping(value="/reset-password", method = RequestMethod.GET)
    public String forgotPassword(Model model) { 
		LOGGER.debug("resetting password now...");
        return "user/resetPassword";
	}
 
	@RequestMapping(value="/reset-password", method = RequestMethod.POST)
	public String changeUserPassword(User user, HttpServletRequest req, Model uiModel) {
 		LOGGER.debug("Inside resetpassword method"); 
		user = userService.loadUserByUsername(user.getEmail());
		 
		if(user != null) {
		Date expireDate = Utility.getPasswordExpireDate();
		user.setTempExpireDate(expireDate);
		userRepository.save(user);
		
		String encodePass = EncryptionService.encodeToNWPasswordReset(user.getEmail());
		//send an email with reset password link
		EmailBean email = new EmailBean(); 
		email.setSubject(EmailConstants.USER_PASSWORD_RESET);
		email.setToEmail(user.getEmail());  
		email.setUser(user); 
		email.setAccountResetLink(resetPasswordLink + encodePass); 
		email.setPasswordResetLink(passwordResetUrl);
		emailService.sendEmail(email, EmailConstants.EMAILBODY_PASSWORD_RESET);  
		uiModel.addAttribute("email", senderEmail);  
	
		}else{
			uiModel.addAttribute("mesg", messages.get(MessageConstants.EMAIL_ALREADY_REGISTERED));  
		}
		
		return "user/resetPassword"; 
	}
	
	@RequestMapping(value="/changePassword", method = RequestMethod.GET)
	public String userPasswordChange(HttpServletRequest req, Model model) {  
		String username = req.getParameter("resetpassword"); 
		String returnPage = "errorPage";  
		User user = userService.loadUserByUsername(EncryptionService.decodeToNWPasswordReset(username)); 
		if(user != null) { 
			if(Utility.isTempPasswordExpired(user.getTempExpireDate())){ 
				returnPage="user/expiredPassword";
			}else{ 
				model.addAttribute("user",user);
				returnPage="user/changePassword";
			} 
		}else{
			model.addAttribute("mesg", messages.get(MessageConstants.CONTACT_ADMIN)); 
		}
		
		return returnPage;

	}
	
	@RequestMapping(value="/updatePassword", method = RequestMethod.GET)
	public String updatePassword(HttpServletRequest request, Model model) {  
		String username = EncryptionService.decodeToNWUserName(request.getParameter("username"));
		String returnPage = "errorPage";  
		User user = userService.loadUserByUsername(username);
		
		String principal = getPrincipal(); 
    	
		Donor donorProfile = donorService.findDonorByUsername(principal); 
		if(user != null && user.getUsername().equalsIgnoreCase(donorProfile.getUser().getUsername())) {  
			model.addAttribute("user",user);
			returnPage="user/changePassword"; 
		}else{
			model.addAttribute("mesg", messages.get(MessageConstants.CONTACT_ADMIN)); 
		}
		
		return returnPage;

	}

	@RequestMapping(value="/changePassword", method = RequestMethod.POST)
	public String userPasswordChangePost(HttpServletRequest req, Model uiModel) {
 
		String emailId = req.getParameter("email");
		String password = req.getParameter("password");
		User existingUser = userService.loadUserByUsername(emailId); 
		if(existingUser != null) {
			existingUser.setPassword(HashGenerator.encryptPassword(password.trim()));  
	 		userRepository.save(existingUser); 
			uiModel.addAttribute("resetMsg", MessageConstants.PASSWORD_CHAGNE_MSG);  
		}
		
		//send email for confirmation about profile changes.
    	EmailBean email = new EmailBean(); 
		email.setUser(existingUser);
		email.setToEmail(existingUser.getEmail()); 
		email.setSubject(EmailConstants.PROFILE_UPDATE_EMAIL_SUBJECT); 
		emailService.sendEmail(email, EmailConstants.PROFILE_UPDATE_EMAIL_TEMPLATE); 
		return "user/login";

	}
  	 
 }
