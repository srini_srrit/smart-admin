 
package com.srrit.web.user.domain;

/**
 * @author Aruna Vemula
 *
 */
public enum UserType {
	
	C, // Customer
	A, // Admin
	P // Provider

}
