package com.srrit.web.user.domain;

public enum Role {
	ROLE_USER,
	ROLE_PROVIDER,
	ROLE_ADMIN
}
