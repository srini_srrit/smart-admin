package com.srrit.web.user.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.srrit.web.common.domain.Address;

@Entity 
@Table (name="donor")
public class Donor implements Serializable { 

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "DONOR_ID") 
	private Integer donorId;   

	private String phone;
	
	private String mobile;

	@Column(name = "PIN_CODE")
 	private String pinCode;
 	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "USER_ID")
	private User user;

	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "ADDRESS_ID")
	private Address address;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_UPDATED")
	private Date lastUpdated;


	public Donor(){
		
	} 	 
	public Integer getDonorId() {
		return donorId;
	}  
	public void setDonorId(Integer donorId) {
		this.donorId = donorId;
	} 
	public String getPinCode() {
		return pinCode;
	} 

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	} 

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	} 

	/* 
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (this.donorId == null || obj == null || !(this.getClass().equals(obj.getClass()))) {
			return false;
		}

		Donor that = (Donor) obj;

		return this.donorId.equals(that.getDonorId());
	}

	/* 
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return donorId == null ? 0 : donorId.hashCode();
	}
	
}
