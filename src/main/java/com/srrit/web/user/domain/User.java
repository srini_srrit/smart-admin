package com.srrit.web.user.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.srrit.web.app.constants.AppConstants;
 
 
@Entity 
public class User implements UserDetails, Serializable {    

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userId;

	private String username; 
	private String password;
	private String email;
    private String firstName;
    private String lastName;
    
    @Transient
    private String fullName;
    
    @Enumerated(EnumType.STRING)
	@Column(name = "user_type")
	private UserType userType;  
	
    private int isVerified; 
	private String updatedUser;
	private int userStatus;  
	
	@Temporal(TemporalType.TIMESTAMP) 
	@Column(name = "CANCEL_DATE")
	private Date cancelDate;
	private int isExpelled; 
	
	@Temporal(TemporalType.TIMESTAMP) 
	@Column(name = "TEMP_EXPIRE_DATE")
	private Date tempExpireDate;

	@Temporal(TemporalType.TIMESTAMP) 
	@Column(name = "CREATION_TIME")
	private Date creationTime;
	 
	@Enumerated(EnumType.STRING)
	@Column(name = "role", length = 20, nullable = false)
	private Role role; 
	
	@Temporal(TemporalType.TIMESTAMP) 
	@Column(name = "LAST_UPDATED")
	private Date lastUpdated;
	
	public User(){
		
	}   

	public Long getUserId() {
		return userId;
	}



	public void setUserId(Long userId) {
		this.userId = userId;
	}



	public String getUsername() {
		return username;
	}

	public void setUsername(String username) { 
		this.username = username;//email.toLowerCase();//.substring(0, email.indexOf("@"));
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) { 
			this.password = password; 
	} 

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName.substring(0, 1).toUpperCase() + firstName.substring(1);
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName.substring(0, 1).toUpperCase() + lastName.substring(1);
	}
	  
	public String getFullName() {
		return getFirstName()+" "+getLastName();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email.toLowerCase();
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public int getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(int isVerified) {
		this.isVerified = isVerified;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

	public int getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(int userStatus) {
		this.userStatus = userStatus;
	} 

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public int getIsExpelled() {
		return isExpelled;
	}

	public void setIsExpelled(int isExpelled) {
		this.isExpelled = isExpelled;
	}  

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}   
	 
	public Date getTempExpireDate() {
		return tempExpireDate;
	}

	public void setTempExpireDate(Date tempExpireDate) {
		this.tempExpireDate = tempExpireDate;
	} 

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(2);
		if(getRole().name() != null) {
			authList.add(new SimpleGrantedAuthority(getRole().name()));
		}
		return authList;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEnabled() { 
		if(AppConstants.INACTIVE_STATUS == getUserStatus()) {
			return true;
		}
		return false;
	} 
}