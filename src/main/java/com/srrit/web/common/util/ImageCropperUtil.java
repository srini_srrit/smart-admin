package com.srrit.web.common.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
  
public class ImageCropperUtil { 
	
	public static BufferedImage getSubImage(byte[] imageInByte, String[] imageCoords) throws IOException {
		// convert byte array back to BufferedImage
		InputStream in = new ByteArrayInputStream(imageInByte);
		BufferedImage originalImage = ImageIO.read(in); 
		BufferedImage SubImgage = originalImage.getSubimage(Integer.valueOf(imageCoords[0]), Integer.valueOf(imageCoords[1]), 
				Integer.valueOf(imageCoords[3]), Integer.valueOf(imageCoords[2]));
		File outputfile = new File("C:/tmp/croppedImage.jpg");
		ImageIO.write(SubImgage, "jpg", outputfile);
		
	 
		return SubImgage;
	}

	/* public static void main(String[] args) {
		try {
			BufferedImage originalImgage = ImageIO.read(new File("C:/tmp/b1_1713.jpg"));
			 
			System.out.println("Original Image Dimension: "+originalImgage.getWidth()+"x"+originalImgage.getHeight());
		 
			ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
			ImageIO.write(originalImgage, "jpg", baos); 
			byte[] bytes = baos.toByteArray(); 
			String str = "0,0,500,600";
			String[] imageCoords = str.split(",");
			BufferedImage SubImgage = getSubImage(bytes, imageCoords);
			System.out.println("Cropped Image Dimension: "+SubImgage.getWidth()+"x"+SubImgage.getHeight());

			File outputfile = new File("C:/tmp/croppedImage.jpg");
			ImageIO.write(SubImgage, "jpg", outputfile);

			System.out.println("Image cropped successfully: "+outputfile.getPath());

		} catch (IOException e) {
			e.printStackTrace();
		}
	} */
}
