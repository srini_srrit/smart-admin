package com.srrit.web.common.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;  

public class ImageFileSizeReducer {  
	
	public static final Log logger = LogFactory.getLog(ImageFileSizeReducer.class);
	public static final  String IMAGE_PATH_LOCATION = "c:\\tmp\\";
	public static final  String IMAGE_EXT = ".jpg";
	
	public void writeImage(String id, String size, byte[] byteImage){   
        try { 
        	logger.debug("writeImage(): Id ->" + id + ", type ->" + size);
            File imagefile = new File(IMAGE_PATH_LOCATION + size +"_"+ id + IMAGE_EXT);
            InputStream in = new ByteArrayInputStream(byteImage);
            BufferedImage bImageFromConvert = ImageIO.read(in);
            ImageIO.write(bImageFromConvert, IMAGE_EXT, imagefile);
            logger.debug("Successfully written the image to image path :" + imagefile.getCanonicalPath());
        } catch (IOException e) {
        	logger.error("Exception occured: writeImage() : name: ->" +id+" ->>>>>>" +e ); 
        } 
	}
	 
	public static void reduceImageFileSize(int sizeThreshold, String srcImg, String destImg) throws Exception {  
		float quality = 1.0f;  

		File file = new File(srcImg);  

		long fileSize = file.length();  

		if (fileSize <= sizeThreshold) {  
			logger.info("Image file size is under threshold");  
			return;  
		}  

		Iterator iter = ImageIO.getImageWritersByFormatName("jpg");  

		ImageWriter writer = (ImageWriter)iter.next();  

		ImageWriteParam iwp = writer.getDefaultWriteParam();  

		iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);  

		FileInputStream inputStream = new FileInputStream(file);  

		BufferedImage originalImage = ImageIO.read(inputStream);  
		IIOImage image = new IIOImage(originalImage, null, null);  

		float percent = 0.1f;   // 10% of 1  

		while (fileSize > sizeThreshold) {  
			if (percent >= quality) {  
				percent = percent * 0.1f;  
			}  

			quality -= percent;  

			File fileOut = new File(destImg);  
			if (fileOut.exists()) {  
				fileOut.delete();  
			}  
			FileImageOutputStream output = new FileImageOutputStream(fileOut);  

			writer.setOutput(output);  

			iwp.setCompressionQuality(quality);  

			writer.write(null, image, iwp);  

			File fileOut2 = new File(destImg);  
			long newFileSize = fileOut2.length();  
			if (newFileSize == fileSize) {  
				// cannot reduce more, return  
				break;  
			} else {  
				fileSize = newFileSize;  
			}  
			logger.info("quality = " + quality + ", new file size = " + fileSize);  
			output.close();  
		}  

		writer.dispose();  
	}  

	public static byte[] reduceImageFileSize(byte[] imageData, String contentType, float quality) throws Exception{
		byte[] medium= null;

		if(imageData != null && imageData.length > 0 && contentType != null) {

			Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName(contentType);  

			ImageWriter writer = iter.next();  

			ImageWriteParam iwp = writer.getDefaultWriteParam();  

			iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);  

			iwp.setCompressionQuality(quality);  

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			
			ImageOutputStream ios = ImageIO.createImageOutputStream(baos);
			
			writer.setOutput(ios);  

			ByteArrayInputStream bais = new ByteArrayInputStream(imageData);

			BufferedImage originalImage = ImageIO.read(bais);  

			IIOImage image = new IIOImage(originalImage, null, null);  
			
			writer.write(null, image, iwp);  
			
			writer.dispose();   
		
			baos.flush();
			
			medium = baos.toByteArray(); 
			 
			baos.close(); 
		}
		return medium;
	} 
	 
}  
