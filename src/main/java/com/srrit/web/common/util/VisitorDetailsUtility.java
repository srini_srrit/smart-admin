package com.srrit.web.common.util;

public class VisitorDetailsUtility {
	
	public static String getPlatformName(String requestHeader) {
	 
     String  userAgent =  requestHeader.toLowerCase();

     String os = ""; 
 
      if (userAgent.toLowerCase().indexOf("windows") >= 0 )
      {
          os = "Windows";
      } else if(userAgent.toLowerCase().indexOf("mac") >= 0)
      {
          os = "Mac";
      } else if(userAgent.toLowerCase().indexOf("x11") >= 0)
      {
          os = "Unix";
      } else if(userAgent.toLowerCase().indexOf("android") >= 0)
      {
          os = "Android";
      } else if(userAgent.toLowerCase().indexOf("iphone") >= 0)
      {
          os = "IPhone";
      }else{
          os = "UnKnown, More-Info: "+userAgent;
      }	
      
      return os;
      
	}
	public static String getBrowserName(String requestHeader) {

	String  userAgent =  requestHeader.toLowerCase();

	String browser ="";
	if (userAgent.contains("msie"))
     {
         String substring=userAgent.substring(userAgent.indexOf("MSIE")).split(";")[0];
         browser=substring.split(" ")[0].replace("MSIE", "IE")+"-"+substring.split(" ")[1];
     } else if (userAgent.contains("safari") && userAgent.contains("version"))
     {
         browser=(userAgent.substring(userAgent.indexOf("Safari")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
     } else if ( userAgent.contains("opr") || userAgent.contains("opera"))
     {
         if(userAgent.contains("opera"))
             browser=(userAgent.substring(userAgent.indexOf("Opera")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
         else if(userAgent.contains("opr"))
             browser=((userAgent.substring(userAgent.indexOf("OPR")).split(" ")[0]).replace("/", "-")).replace("OPR", "Opera");
     } else if (userAgent.contains("chrome"))
     {
         browser=(userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0]).replace("/", "-");
     } else if ((userAgent.indexOf("mozilla/7.0") > -1) || (userAgent.indexOf("netscape6") != -1)  || (userAgent.indexOf("mozilla/4.7") != -1) || (userAgent.indexOf("mozilla/4.78") != -1) || (userAgent.indexOf("mozilla/4.08") != -1) || (userAgent.indexOf("mozilla/3") != -1) )
     {
         //browser=(userAgent.substring(userAgent.indexOf("MSIE")).split(" ")[0]).replace("/", "-");
         browser = "Netscape-?";

     } else if (userAgent.contains("firefox"))
     {
         browser=(userAgent.substring(userAgent.indexOf("Firefox")).split(" ")[0]).replace("/", "-");
     } else if(userAgent.contains("rv"))
     {
         browser="IE-" + userAgent.substring(userAgent.indexOf("rv") + 3, userAgent.indexOf(")"));
     } else
     {
         browser = "UnKnown, More-Info: "+userAgent;
     }
	
		return browser;
	} 
	
	public static void main(String[] args) {
		System.out.println(getBrowserName("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36"));
	}
}
