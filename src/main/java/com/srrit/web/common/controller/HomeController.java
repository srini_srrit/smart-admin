package com.srrit.web.common.controller;

 import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
 
 	@GetMapping("/")
	public String welcome(Model uiModel) { 
 	 	return "index";
	} 	 

}
