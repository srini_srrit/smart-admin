package com.srrit.web.common.controller;
 
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller; 

@Controller
public abstract class AbstractController {  
	 
	public UserDetails getCurrentUser(Authentication authentication) {
	        return (authentication == null) ? null : (UserDetails) authentication.getPrincipal();
	   } 
	
	 public String getPrincipal(){
	        String userName = null;
	        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	 
	        if (principal instanceof UserDetails) {
	            userName = ((UserDetails)principal).getUsername();
	        } else {
	            userName = principal.toString();
	        }
	        return userName;
	    }  
}
