package com.srrit.web.common.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StaticContentController extends AbstractController{  
	  
	public static final Log logger = LogFactory.getLog(StaticContentController.class);
	

	@Value("${SENDER_USERNAME}")
    private String senderEmail;

	@GetMapping(value="terms")
	 public String termsPage(Model model){ 
	     logger.debug(" .termsPage()..."); 
		 return "common/terms";
	    }
	 
	 @GetMapping(value="faqs")
	 public String faqsPage(Model model){ 
	     logger.debug(" .faqsPage()..."); 
		 return "common/faqs";
	    }
	 
	 @GetMapping(value="privacy")
	    public String privacyPage(Model model){ 
		  logger.debug("privacyPage()...");     
		 return "common/privacy";
	    
	 } 

	 @GetMapping(value="about-us")
	 public String aboutUSPage(Model model){ 
	     logger.debug(" .aboutUSPage()..."); 
		 return "common/aboutus";
	    }
	@GetMapping(value = "coming-soon")
	public String comingSoonMapping(){
	        return "comingSoon";
	     }

}
