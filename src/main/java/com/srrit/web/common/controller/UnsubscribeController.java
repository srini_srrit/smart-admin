
/**
 * 
 */
package com.srrit.web.common.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.srrit.web.app.constants.AppConstants;
import com.srrit.web.app.constants.EmailConstants;
import com.srrit.web.app.constants.MessageConstants;
import com.srrit.web.common.bean.EmailBean;
import com.srrit.web.common.configuration.B4LMessages;
import com.srrit.web.common.domain.Subscribe;
import com.srrit.web.common.repository.SubscribeRepository;
import com.srrit.web.common.service.EmailService;
import com.srrit.web.common.service.EncryptionService;
import com.srrit.web.common.service.SubscribeService;
 
/**
 * @author Aruna Vemula Vemula
 *
 */
@Controller
public class UnsubscribeController extends AbstractController { 
 	
 	private static final Logger LOGGER = LoggerFactory.getLogger(UnsubscribeController.class); 
	 
	@Autowired
    private EmailService emailService;
 	
	@Value("${account.resetpassword}")
    private String resetPasswordLink; 
	
	@Value("${account.verifylink}")
    private String verifylink; 
	
	@Value("${app.url}")
    private String siteUrl;  
	 
	@Value("${account.passwordreseturl}")
    private String passwordResetUrl;  
 
	@Autowired
	private SubscribeRepository subscribeRepository;
	
	@Autowired
    private SubscribeService subscribeService;
	
	@Autowired
    private B4LMessages messages; 

	@RequestMapping(value = "subscribe-email", method = RequestMethod.POST)
     public String subscribeEmail(@RequestParam(value="emailId") String email, Model model){  
		
		Subscribe subscribe = subscribeService.getSubscribeByEmail(email.trim());
		if(subscribe == null){
			subscribe = new Subscribe();
			 
			subscribe.setEmail(email); 
			subscribe.setStatus(AppConstants.ACTIVE_STATUS);
			subscribeRepository.save(subscribe); 
			
			EmailBean emailBean = new EmailBean(); 
			emailBean.setSubject(EmailConstants.SUBSCRIBE_EMAIL_SUBJECT);
			emailBean.setToEmail(email);
			
			emailService.sendEmail(emailBean, EmailConstants.EMAILBODY_SUBSCRIBE);  
			model.addAttribute("mesg", messages.get(MessageConstants.SUBSCRIBE_MESG));
		
		}else if(subscribe.getStatus() == 0) {
			subscribe.setStatus(AppConstants.ACTIVE_STATUS);
			subscribeRepository.save(subscribe); 
			model.addAttribute("mesg", messages.get(MessageConstants.SUBSCRIBE_MESG_USER_ACTIVATED));
		}else{ 
			model.addAttribute("mesg", messages.get(MessageConstants.SUBSCRIBE_MESG_USER_EXISTS));
		}
		
        return "success";
     } 
	
	@RequestMapping(value = "unsubscribeEmail")
     public String unsubscribeEmail(@RequestParam(value="email") String email, Model model){  
 		if(email != null) {
			Subscribe subscribe = subscribeService.findSubscribeByEmail(EncryptionService.decodeStandard(email.trim()));
			if(subscribe != null) {
				subscribe.setStatus(AppConstants.INACTIVE_STATUS);
				subscribeRepository.save(subscribe);
				model.addAttribute("mesg", messages.get(MessageConstants.UNSUBSCRIBE_MESG));
				return "success"; 
				
			}else{
				model.addAttribute("mesg", messages.get(MessageConstants.UNSUBSCRIBE_MESG_ERROR));
				return "success"; 
			}
 		}
 		return "errorPage";
     } 
	
	@RequestMapping(value = "unsubscribe", method = RequestMethod.GET)
    public String unsubscribe(Model model){  
		Subscribe unsubscribe = new Subscribe();
		model.addAttribute("subscribe", unsubscribe); 
		return "unsubscribe";
    } 
 
	@RequestMapping(value="unsubscribe", method = RequestMethod.POST)
	public String unsubscribe(Subscribe unsubscribe, Model uiModel) {
 		LOGGER.debug("Inside unsubscribe method"); 
 		List<Subscribe> list = subscribeService.findSubscribeByEmailIds(unsubscribe.getEmail().trim());
		String mesg = ""; 
		if(list != null && list.size()> 0) {
			
			for(Subscribe subscribe: list){
			subscribe.setStatus(AppConstants.INACTIVE_STATUS);
			subscribeRepository.save(subscribe);
			}
			mesg = messages.get(MessageConstants.UNSUBSCRIBE_MESG);
		}else{  
			unsubscribe.setStatus(AppConstants.INACTIVE_STATUS);
			subscribeRepository.save(unsubscribe);
			mesg = messages.get(MessageConstants.UNSUBSCRIBE_MESG_ERROR);
		}
	 	uiModel.addAttribute("mesg", mesg);  
	 	
		return "success"; 
	}
}
