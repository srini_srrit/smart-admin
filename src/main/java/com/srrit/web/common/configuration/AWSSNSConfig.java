package com.srrit.web.common.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions; 
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;

@Configuration
public class AWSSNSConfig {
	
	@Value("${b4l.aws.access_key_id}")
	private String awsId;

	@Value("${b4l.aws.secret_access_key}")
	private String awsKey;
	
	@Value("${b4l.sns.region}")
	private String region; 
	
	@Bean
	public AmazonSNS snsClient() {
		
		BasicAWSCredentials awsCreds = new BasicAWSCredentials(awsId, awsKey);
		AmazonSNS snsClient = AmazonSNSClientBuilder.standard()
								.withRegion(Regions.fromName(region))
		                        .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
		                        .build();
		
		return snsClient;
	}
}
