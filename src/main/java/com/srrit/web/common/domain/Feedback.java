
/**
 * 
 */
package com.srrit.web.common.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Aruna Vemula
 *
 */
@Entity 
public class Feedback {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "FEEDBACK_ID")
	private Integer feedbackId;
	 
	private String name;
	private String email; 
	private String subject;
	
	@Column(name = "EXISTUSER")
	private String existUser;
	private String message;
	
	@Temporal(TemporalType.TIMESTAMP) 
	@Column(name = "LAST_UPDATED")
	private Date lastUpdated;
	
	public final Integer getFeedbackId() {
		return feedbackId;
	}
	public final void setFeedbackId(Integer feedbackId) {
		this.feedbackId = feedbackId;
	}
	public final String getName() {
		return name;
	}
	public final void setName(String name) {
		this.name = name.substring(0, 1).toUpperCase() + name.substring(1);;
	}
	public final String getEmail() {
		return email;
	}
	public final void setEmail(String email) {
		this.email = email;
	}
	public final String getSubject() {
		return subject;
	}
	public final void setSubject(String subject) {
		this.subject = subject;
	}
	public final String getExistUser() {
		return existUser;
	}
	public final void setExistUser(String existUser) {
		this.existUser = existUser;
	}
	public final String getMessage() {
		return message;
	}
	public final void setMessage(String message) {
		this.message = message;
	}
	public final Date getLastUpdated() {
		return lastUpdated;
	}
	public final void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	 

}
