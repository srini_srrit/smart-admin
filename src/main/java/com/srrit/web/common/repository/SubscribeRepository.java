package com.srrit.web.common.repository;

import org.springframework.data.repository.CrudRepository;

import com.srrit.web.common.domain.Subscribe;
 
public interface SubscribeRepository extends CrudRepository<Subscribe, Integer>{
	   
}
