package com.srrit.web.common.repository;

import org.springframework.data.repository.CrudRepository;

import com.srrit.web.common.domain.Feedback;

public interface FeedbackRepository extends CrudRepository<Feedback, Integer>{
	   
}
