package com.srrit.web.common.service;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.codec.Base64;
 
public class EncryptionService {
	
	 private static final Logger logger = LoggerFactory.getLogger(EncryptionService.class);

	public static final String B4LENCDSPCL_CODE = "snw76";
	public static final String B4LENCDSPCL_RESET_PASSWORD = "pnw70";
	public static final String B4LNCDSPCL_USERCODE = "unw";

	public static void main(String[] args) {
		System.out.println(encodeStandard("spandana2018"));
	}
	
	public static boolean isValidString(String... str) {
		if (str == null) {
			return false;
		}
		for (String strEle : str) { 
			if (strEle == null || "".equals(strEle)) {
				return false;
			}
		}
		return true;
	}

	public static boolean isEqual(String str1, String str2) {
		if (isValidString(str1, str2) && str1.equals(str2)) {
			return true;
		}
		return false;
	}

	public static String encodeStandard(String string) {
		String returnString = null;

		if (isValidString(string)) {
			try {

				byte[] encoded = Base64.encode((string + B4LENCDSPCL_CODE).getBytes());
				returnString = new String(encoded);

			} catch (Exception e) {
				logger.error("Exception occured: encodeToNWStandard() : string: ->" +string+" ->>>>>>" +e ); 
		        
			}
		}
		return returnString;
	}

	public static String decodeStandard(String string) {
		String returnString = null;

		if (isValidString(string)) {

			try {

				returnString = new String(Base64.decode((string + B4LENCDSPCL_CODE).getBytes()));

				int i = returnString.indexOf(B4LENCDSPCL_CODE);

				if (i >= 0) {
					if (returnString != null) {
						returnString = returnString.substring(0, i);
					}
				}

			} catch (Exception e) {
				logger.error("Exception occured: decodeToNWStandard() : string: ->" +string+" ->>>>>>" +e ); 
			}
		}
		return returnString;
	}

	public static String encodeToNWPasswordReset(String string) {
		String returnString = null;

		if (isValidString(string)) {

			try {
				returnString = new String(Base64.encode((string + B4LENCDSPCL_RESET_PASSWORD).getBytes()));

			} catch (Exception e) {
				logger.error("Exception occured: encodeToNWPasswordReset() : string: ->" +string+" ->>>>>>" +e ); 
			}
		}
		return returnString;
	}

	public static String decodeToNWPasswordReset(String string) {
		String returnString = null;

		if (isValidString(string)) {

			try {
				returnString = new String(Base64.decode(string.getBytes()));
				int i = returnString.indexOf(B4LENCDSPCL_RESET_PASSWORD);
				if (i >= 0) {
					if (returnString != null) {
						returnString = returnString.substring(0, i);
					}
				}

			} catch (Exception e) {
				logger.error("Exception occured: decodeToNWPasswordReset() : string: ->" +string+" ->>>>>>" +e ); 
			}
		}
		return returnString;
	}
	
	public static String encodeToNWUserName(String string) {
		String returnString = null;
		if(isValidString(string)) {
			try {
			    int i = string.length();
			    if(i >= 0) {
			    	String firstHalf = string.substring(0, i/2);
			    	String secondHalf = string.substring(i/2, i);
			    	String finalString = secondHalf+B4LNCDSPCL_USERCODE+firstHalf;
			    	returnString = new String(Base64.encode(finalString.getBytes()));
			    }
			} catch (Exception e) {
				logger.error("Exception occured: encodeToNWUserName() : string: ->" +string+" ->>>>>>" +e );
			}
		}
		
		return returnString;
	}
	
	public static String decodeToNWUserName(String string) {
		String returnString = null;
		if(isValidString(string)) {
			try {
				String decodedString = new String(Base64.decode(string.getBytes()));
			    int i = decodedString.length();
			    int keyIndex = decodedString.indexOf(B4LNCDSPCL_USERCODE);
			    if(i >= 0 && keyIndex >= 0) {
			    	String firstHalf = decodedString.substring(keyIndex+3);
			    	String secondHalf = decodedString.substring(0, keyIndex);
			    	returnString = firstHalf+secondHalf;
			    }
			} catch (Exception e) {
				logger.error("Exception occured: decodeToNWUserName() : string: ->" +string+" ->>>>>>" +e );
			}
		}
		
		return returnString;
	}

	public boolean authenticate(String attemptedPassword, byte[] encryptedPassword, byte[] salt)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		// Encrypt the clear-text password using the same salt that was used to
		// encrypt the original password
		byte[] encryptedAttemptedPassword = getEncryptedPassword(attemptedPassword, salt);

		// Authentication succeeds if encrypted password that the user entered
		// is equal to the stored hash
		return Arrays.equals(encryptedPassword, encryptedAttemptedPassword);
	}

	public byte[] getEncryptedPassword(String password, byte[] salt)
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		// PBKDF2 with SHA-1 as the hashing algorithm. Note that the NIST
		// specifically names SHA-1 as an acceptable hashing algorithm for
		// PBKDF2
		String algorithm = "PBKDF2WithHmacSHA1";
		// SHA-1 generates 160 bit hashes, so that's what makes sense here
		int derivedKeyLength = 160;
		// Pick an iteration count that works for you. The NIST recommends at
		// least 1,000 iterations:
		// http://csrc.nist.gov/publications/nistpubs/800-132/nist-sp800-132.pdf
		// iOS 4.x reportedly uses 10,000:
		// http://blog.crackpassword.com/2010/09/smartphone-forensics-cracking-blackberry-backup-passwords/
		int iterations = 20000;

		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, derivedKeyLength);

		SecretKeyFactory f = SecretKeyFactory.getInstance(algorithm);

		return f.generateSecret(spec).getEncoded();
	}

	public byte[] generateSalt() throws NoSuchAlgorithmException {
		// VERY important to use SecureRandom instead of just Random
		SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

		// Generate a 8 byte (64 bit) salt as recommended by RSA PKCS5
		byte[] salt = new byte[8];
		random.nextBytes(salt);

		return salt;
	} 
}
