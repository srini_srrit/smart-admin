package com.srrit.web.common.service;

import com.srrit.web.common.bean.EmailBean;

public interface EmailService {
	 
	void sendEmail(EmailBean email, String  template);  
	
}

