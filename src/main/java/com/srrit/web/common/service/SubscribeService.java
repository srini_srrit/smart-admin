package com.srrit.web.common.service;

import java.util.List;

import com.srrit.web.common.domain.Subscribe;
 

public interface SubscribeService {

	Subscribe findSubscribeByEmail(String email);
	
	Subscribe getSubscribeByEmail(String email);
	
	List<Subscribe> findSubscribeByEmailIds(String email);
}
