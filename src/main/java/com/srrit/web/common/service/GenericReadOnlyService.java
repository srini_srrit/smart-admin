package com.srrit.web.common.service;

import java.util.List;

public interface GenericReadOnlyService<E, T> {
	
	List<E> fetchAllRecords();

	E getEntityById(T pk);
}
