package com.srrit.web.common.service;
 
import java.io.StringWriter; 
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.apache.velocity.tools.generic.DateTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;  
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator; 
import org.springframework.stereotype.Service; 

import com.srrit.web.app.constants.EmailConstants;
import com.srrit.web.common.bean.EmailBean;
   
@Service
public class EmailServiceImpl implements EmailService {

    private static final Log logger = LogFactory.getLog(EmailServiceImpl.class);
 
    @Autowired
    private JavaMailSender mailSender;

    @Value("${SENDER_USERNAME}")
    private String senderEmail; 
    
    @Value("${app.url}")
    private String siteUrl;  
 
    
    @Override
    public void sendEmail(EmailBean email, String templateFile) { 
    	
    	Properties properties = new Properties();
        properties.setProperty("resource.loader", "class");
        properties.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.init(properties);
        
    	MimeMessagePreparator preparator = new MimeMessagePreparator() {
              public void prepare(MimeMessage mimeMessage) throws Exception {
            	  
            	  try {
            		  MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            		  
            		  if(email.getToEmail() != null) {
            			  message.setTo(email.getToEmail()); 
            		  } 
            		 
            		  if(email.getEmailIds() != null) {
            			  message.setTo(email.getEmailIds());
            		  }
            	 	  if(email.getCcCopy() != null) {
            			  message.setCc(email.getCcCopy());
            		  }
            		  if(email.getBcCopy() != null) {
            			  message.setBcc(email.getBcCopy());
            		  } 
            		  message.setSubject(email.getSubject());  
            		  email.setEmailFrom(senderEmail); 
            		  email.setSiteUrl(siteUrl);
            		  email.setContactUsLink(siteUrl + EmailConstants.CONTACTUS);
            		  VelocityContext model = new VelocityContext(); 
            		  model.put("email", email); 
            		  model.put("date", new DateTool());   

            		  Template template = velocityEngine.getTemplate(templateFile);
            		  StringWriter writer = new StringWriter();
            		  template.merge(model, writer);  


            		  message.setText(writer.toString(), true); 

            	  }catch(VelocityException ve){
            		  logger.error("Exception occured while sending email: " + ve.getMessage() + ",template :"
            	  + templateFile +", email :" + email.getToEmail() +", subject: "+ email.getSubject());
 
            	  }
              }
          }; 
          
          this.mailSender.send(preparator);  
          
          logger.debug("email sent successfully");
    }  
}
