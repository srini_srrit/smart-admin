package com.srrit.web.app.constants;

public interface MessageConstants {
	
	public static final String PASSWORD_CHAGNE_MSG = "Your password has been changed successfully";
	public static final String ACTIVATED_SUCCESS_MSG = "Thanks for activating your account. You can now login";
	public static final String HEADLINE_SCROLL_TEXT = "main.scroll.text";
	public static final String BANNER_TEXT = "main.headline.text"; 
	
	public static final String CONTACTUS_MESG = "contactus.mesg"; 
	public static final String SUBSCRIBE_MESG = "subscribe.mesg";
	public static final String UNSUBSCRIBE_MESG = "unsubscribe.mesg";
	public static final String UNSUBSCRIBE_MESG_ERROR = "unsubscribe.mesg.error";
	public static final String SUBSCRIBE_MESG_USER_EXISTS = "subscribe.mesg.user.exits";
	public static final String SUBSCRIBE_MESG_USER_ACTIVATED = "subscribe.mesg.user.activated";
	public static final String DATA_NOT_FOUND = "data.notfound";  
	
	public static final String USER_NOT_FOUND = "user.notfound";
	public static final String INVALID_USER = "invalid.user";
	public static final String USER_NOT_VERIFIED = "user.notverified";
	public static final String USER_DISABLED = "user.disabled";
	public static final String EMAIL_ALREADY_REGISTERED = "email.already.registed";
	
	public static final String CONTACT_ADMIN = "contact.admin";  
 	public static final String ADS_CANCEL_MESG = "ads.cancel.mesg";
 	public static final String ADS_UPDATE_MESG = "ads.update.mesg";
	public static final String PROFILE_UPDATE_MESG = "profile.update.mesg";
	public static final String UPDATE_MESG = "update.mesg";
	public static final String CONTACT_ADV_MESG = "contact.adv.mesg"; 
	public static final String NO_RESULTS_FOUND_MESG= "no.results.found"; 
	public static final String DISCLAIMER_NOTE_MESG= "disclaimer.note";  
	
	public static final String EMAIL_REGISTERED = "email.registed";
	public static final String SIGNUP_SUCCESS_MESG = "singup.success.mesg";
	
	public static final String SOCIAL_LOGIN_ERROR = "social.login.error"; 
	 
	public static final String NO_AD_POST_FOUND="no.ads.found";
	
	public static final String DISCOUNT_OFFER_MESG = "discount.offer.mesg";
	public static final String RECAPTCHA_ERROR_MESG = "recaptcha.error.mesg";
	public static final String SMS_TEXT_MESG = "sms.text.mesg";
	
	 
	
	
}
