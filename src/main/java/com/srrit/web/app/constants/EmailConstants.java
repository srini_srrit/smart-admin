package com.srrit.web.app.constants;

public interface EmailConstants {

	public static final String USER_WELCOME = "Thank you message for Blood Donation & Registration";

	public static final String USER_WELCOME_SUBJECT = "Thank you for visiting our site";  
 
	public static final String VERIFY_ACCOUNT_SUBJECT = "Blood4Life.org: Verify your account";
	public static final String USER_PASSWORD_RESET = "Requested Password Reset for Your Blood4Life.org account";

	public static final String FEEDBACK_SUBJECT = "Customer Feedback";   
	
	public static final String CONTACTUS_EMAIL_SUBJECT = "Blood4Life.org: Contact Us info";   
	public static final String PROFILE_UPDATE_EMAIL_SUBJECT = "Your Blood4Life.org account has been updated";
	public static final String UNSUBSCRIBE_EMAIL_SUBJECT = "Re: your unsubscribe request"; 
	
	public static final String SUBSCRIBE_EMAIL_SUBJECT = "Welcome to Blood4Life.org !";  
	
	public static final String EMAILBODY_USER_WELCOME = "email/welcome.vm";   
	public static final String EMAILBODY_VERIFY_ACCOUNT = "email/verifyAccount.vm";
	public static final String EMAILBODY_PASSWORD_RESET = "email/resetPassword.vm";  

	public static final String EMAILBODY_FEEDBACK = "email/feedback.vm";
	
	public static final String EMAILBODY_SUBSCRIBE = "email/subscribe.vm";
	
	public static final String EMAILBODY_UNSUBSCRIBE = "email/unsubscribe.vm";
	 
	public static final String EXPELLUSER_TEMPLATE = "email/expelluser.vm";
	public static final String UNDOEXPELLUSER_TEMPLATE = "email/undoexpelluser.vm";
	public static final String EMAILACTIVEUSERS_TEMPLATE = "email/emailallactiveusers.vm";
	public static final String CONTACTUS_EMAIL_TEMPLATE = "email/contactusemail.vm";  
 	public static final String PROFILE_UPDATE_EMAIL_TEMPLATE = "email/profile.vm";  
 	
	public static final String DONOTREPLY = "***PLEASE NOTE THIS IS A NO REPLY MAIL***";
	
	public static final String emailTo ="contactus@spandnaa.org";
	
	public static final String emailFrom ="contactus@spandnaa.org";
	
	public static final String CONTACTUS="contact-us";
	
	public static final String EMAIL_ATTACHMENT_LOGO = "email/logo.png";
	
	public static final String REQUESTING_FOR_DONORS = "Urgent blood needed";
	
	public static final String REQUESTING_FOR_DONORS_TEMP = "email/requestDonor.vm";
	 
}
