package com.srrit.web.app.constants;

public interface SEOMessageConstants {
	
	public static final String DEFAULT_TITLE ="default.title"; 
	public static final String DEFAULT_META_DESC ="default.meta.desc"; 
	public static final String DEFAULT_META_KEYWORDS ="default.meta.keywords"; 

	public static final String TITLE =".title"; 
	public static final String META_DESC =".meta.desc"; 
	public static final String META_KEYWORDS =".meta.keywords";  
}
 