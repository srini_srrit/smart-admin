<%@ include file="/WEB-INF/jsp/include/includes.jsp"%>   
       <!-- START FOOTER  -->

        <footer>            

            <section class="footer-widget-area footer-widget-area-bg">

                <div class="container"> 
                    <div class="row"> 						                      

                        <div class="col-md-4 col-sm-6 col-xs-12">

                            <div class="footer-widget">

                                <div class="sidebar-widget-wrapper">

                                    <div class="footer-widget-header clearfix">
                                        <h3>Contact Us</h3>
                                    </div>  <!--  end .footer-widget-header --> 


                                    <div class="textwidget">                                       

                                        <i class="fa fa-envelope-o fa-contact"></i> <p><a href="mailto:blood4life@spandana.org">blood4life@spandana.org</a><br/></p>
 
                                        <i class="fa fa-phone fa-contact"></i> <p>Office:&nbsp;<a href="tel:+12672166200"> (267) 216-6200</p>

                                    </div>

                                </div> <!-- end .footer-widget-wrapper  -->

                            </div> <!--  end .footer-widget  -->

                        </div> <!--  end .col-md-4 col-sm-12 -->   

                        <div class="col-md-4 col-sm-12 col-xs-12">

                            <div class="footer-widget clearfix">

                                <div class="sidebar-widget-wrapper">

                                    <div class="footer-widget-header clearfix">
                                        <h3>Support Links</h3>
                                    </div>  <!--  end .footer-widget-header --> 


                                    <ul class="footer-useful-links">

                                        <li>
                                            <a href="https://www.spandana.org">
                                                <i class="fa fa-caret-right fa-footer"></i>
                                                Spandana
                                            </a>
                                        </li>                       

                                    </ul>

                                </div> <!--  end .footer-widget  -->        

                            </div> <!--  end .footer-widget  -->            

                        </div> <!--  end .col-md-4 col-sm-12 -->   

                    </div> <!-- end row  -->

                </div> <!-- end .container  -->

            </section> <!--  end .footer-widget-area  -->

            <!--FOOTER CONTENT  -->

            <section class="footer-contents">

                <div class="container">

                    <div class="row clearfix">
                        
                        <div class="col-md-12 col-sm-12 text-center">
                            <p class="copyright-text"> Copyright 2019 © Spandana.org. All Rights Reserved.</p>
                        </div>  <!-- end .col-sm-6  -->

                    </div> <!-- end .row  -->                                    

                </div> <!--  end .container  -->

            </section> <!--  end .footer-content  -->

        </footer>

        <!-- END FOOTER  -->

        <!-- Back To Top Button  -->

		<a id="backTop">Back To Top</a>
		<script src="${basePath}js/jquery.min.js"></script> 
		<script src="${basePath}js/bootstrap.min.js"></script>    
		<script src="${basePath}js/wow.min.js"></script>
		<script src="${basePath}js/jquery.backTop.min.js "></script>
		<script src="${basePath}js/waypoints.min.js"></script>
		<script src="${basePath}js/waypoints-sticky.min.js"></script> 
		<script src="${basePath}js/owl.carousel.min.js"></script> 
		<script src="${basePath}js/jquery.stellar.min.js"></script>
		<script src="${basePath}js/jquery.counterup.min.js"></script>
		<script src="${basePath}js/venobox.min.js"></script>
        <script type="text/javascript" src="${basePath}js/formValidation.min.js"></script>

		<script src="${basePath}js/custom-scripts.js"></script>  
  	 		<script src="${basePath}js/jquery.dataTables.min.js"></script>
 		<script src="${basePath}js/dataTables.bootstrap.min.js"></script> 
		
		
    </body>

</html>