<!DOCTYPE html>
<html lang="en">
<%@ include file="/WEB-INF/jsp/include/includes.jsp"%>
        <head>
       <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="format-detection" content="telephone=no">
	    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0" />
	    <meta name="apple-mobile-web-app-capable" content="yes" />
        <title>Spandana Blood4Life - Blood Donation Campaign</title> 
        <meta name="description" content="Reddrop Buddies - Blood Donation Campaign & Multi-Concept Activism Template">
        <meta name="author" content="xenioushk">
        <link rel="shortcut icon" href="images/favicon.png" />

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- The styles -->
        <link rel="stylesheet" href="${basePath}css/bootstrap.css" />
        <link href="${basePath}css/font-awesome.css" rel="stylesheet" type="text/css" >
        <link href="${basePath}css/animate.css" rel="stylesheet" type="text/css" >
        <link href="${basePath}css/owl.carousel.css" rel="stylesheet" type="text/css" >
        <link href="${basePath}css/venobox.css" rel="stylesheet" type="text/css" >
        <link rel="stylesheet" href="${basePath}css/styles.css" />
          <!-- Theme CSS -->
          <link rel="stylesheet" href="${basePath}css/formValidation.min.css">
         <link rel="stylesheet" href="${basePath}css/blood4life.css" type="text/css"/>
          <link rel="stylesheet" href="${basePath}css/jquery.dataTables.min.css" type="text/css"/> 
          

    <body>  

        <!--  HEADER -->

        <header class="main-header clearfix"> 
            <div class="top-bar clearfix"> 
                <div class="container">  
                    <div class="row"> 
                        <div class="col-md-8 col-sm-12"> 
                            <p>
                                <span><i class="fa fa-envelope-o fa-contact"></i> <strong>Contact: </strong>  contactus@spandnaa.org</span>
                                <span>&nbsp;<i class="fa fa-phone"></i> <strong>Call Us:</strong> +1 (561) 319 6359</span>
                            </p> 
                        </div> 
                        <div class="col-md-4col-sm-12">
                            <div class="top-bar-social">
                                <a href="#"><i class="fa fa-facebook rounded-box"></i></a>
                                <a href="#"><i class="fa fa-twitter rounded-box"></i></a>
                                <a href="#"><i class="fa fa-google-plus rounded-box"></i></a>
                                <a href="#"><i class="fa fa-instagram rounded-box"></i></a>
                                <a href="#"><i class="fa fa-youtube rounded-box"></i></a>
                            </div>   
                        </div> 

                    </div>

                </div> <!--  end .container -->

            </div> <!--  end .top-bar  -->

            <section class="header-wrapper navgiation-wrapper">

                <div class="navbar navbar-default">			
                    <div class="container">

                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="logo" href="${baseUrl}"><img alt="" src="${staticPath}images/logo.gif"></a>
                        </div>

                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="${baseUrl}">Home</a>
                                </li>
                                
                                <li><a href="#" title="About Us">About Us</a></li>

                                <li>
                                    <a href="#">Campaign</a> 
                                </li> 

                                <li>
                                    <a href="#">Blog</a> 
                                </li>

                                <li><a href="${baseUrl}contact-us">Contact</a></li>
                                   <sec:authorize access="authenticated" var="isAuthenticated" />
							<sec:authentication var="user" property="principal" />
							<c:choose>
								<c:when test="${isAuthenticated}">
								 	 <li class="drop"><a href="#">My Account</a>
                                        <ul class="drop-down">
                                             <li class="drop"><a href="#">My Profile</a>
                                              <li class="drop"><a href="${baseUrl}donorsList">Donors List</a>
								             <li class="drop"><a href="${baseUrl}logout">Logout</a></li>
								      <ul>
								   </li>
								</c:when>
								<c:otherwise>
									<li><a href="${baseUrl}login">Sign In</a></li>
								</c:otherwise>
							</c:choose>
						</ul>
                        </div>
                    </div>
                </div>

            </section>

        </header> <!-- end main-header  -->