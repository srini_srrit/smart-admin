<!DOCTYPE html>
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>
	<div class="container main-wrapper">
    	<div class="main-content mag-content clearfix">
    		<h3 class="small-title">
    			<span>User List</span>
    		</h3>
    		<br />
			<div class="table-responsive"> 
        	<table class="table table-striped col-md-12">
            <tr>
                <th>Id</th>
                <th>First Name</th> 
                 <th>Last Name</th>
                <th>Email</th>
                <th>Blood Group</th>
                <th>Location</th>
                <th>isVerified</th>
                <th>Enable/Disable</th> 
            </tr>
            <c:forEach items="${userList}" var="user">
               <spring:url var="editUrl" value="${baseDir}userEdit">
						<spring:param name="id" value="${user.userId}" />
				   </spring:url>
				     <spring:url var="deleteUrl" value="${baseDir}userDelete">
						<spring:param name="id" value="${user.userId}" />
				   </spring:url>
				     <spring:url var="detailUrl" value="${baseDir}userDetail">
						<spring:param name="id" value="${user.userId}" />
				   </spring:url>
            <tr>
                <td><a href="${detailUrl}"><c:out value="${user.userId}"/></a></td>
                <td><c:out value="${user.firstName}"/></td> 
                 <td><c:out value="${user.lastName}"/></td>
                  <td><c:out value="${user.email}"/></td>
                    <td><c:out value="${user.email}"/></td>
                      <td><c:out value="${user.email}"/></td>
                <c:choose>
							<c:when test="${user.isVerified == 1}">
							 <td><a href="${activateUrl}">Active</a></td>
               			 	</c:when>
               			 	<c:otherwise>
               				 <td><a href="${deleteUrl}">Not Active</a></td> 
						  	</c:otherwise>
               			 	</c:choose> 
               	<%-- 
                 <td><c:out value="${user.isVerified}"/></td> 
                  <td><c:out value="${user.userStatus}"/></td>  --%> 
             	<%--      <td><c:out value="${user.role}"/></td>  --%>
                     <c:choose>
							<c:when test="${user.userStatus == 0}">
							 <td><a href="${editUrl}">Edit</a></td>
               			 	</c:when>
               			 	<c:otherwise>
               				 <td><a href="${deleteUrl}">Disable</a></td> 
						  	</c:otherwise>
               			 	</c:choose> 
            </tr>
            </c:forEach>
        </table>
		</div>
		</div>
		</div>
<%@ include file="/WEB-INF/jsp/include/footer.jsp"%>