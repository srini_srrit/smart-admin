<!DOCTYPE html> 
<%@ include file="/WEB-INF/jsp/include/header.jsp"%> 
<div class="container main-wrapper">
              	<div class="main-content mag-content clearfix">
              		<h3 class="small-title">
              			<span>Donor Registration</span>
              		</h3>
              		<br />
		<form:form method="post" name="userSignupForm" id="userSignupForm"
			modelAttribute="donor" action="${baseDir}signup" class="form-horizontal">
			<form:hidden path="donorId" />
			<c:if test="${not empty error}">
				<div class="row">
					<div class="col-md-8">
						<div class="alert alert-danger">
							<p>${error}</p>
						</div>
					</div>
				</div>
			</c:if>
			<c:if test="${not empty isExisting && isExisting eq 'true'}">
				<div class="row">
					<div class="col-sm-12">
						<div class="col-sm-4  text-center">
							Already have an account?<a href="${baseUrl}login"> Sign In </a><br />
							<br />
						</div>
					</div>
				</div>
			</c:if>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label for="firstName" class="col-sm-2 control-label">First
							Name:</label>
						<div class="col-sm-4">
							<form:input path="user.firstName" id="firstName"
								class="form-control" placeholder="First Name *" />
							<form:errors path="user.firstName" cssClass="error" /> 
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label for="lastName" class="col-sm-2 control-label">Last
							Name:</label>
						<div class="col-sm-4">
							<form:input path="user.lastName" id="lastName"
								class="form-control" placeholder="Last Name *" />
								<form:errors path="user.lastName" cssClass="error" /> 
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label for="email" class="col-sm-2 control-label">Email:</label>
						<div class="col-sm-4">
							<form:input path="user.email" id="email" class="form-control"
								placeholder="Email *" />
							<form:errors path="user.email" cssClass="error" />	
								
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label for="password" class="col-sm-2 control-label">Password:</label>
						<div class="col-sm-4">
							<form:password path="user.password" id="password"
								class="form-control" placeholder="Password *" />
							<form:errors path="user.password" cssClass="error" />		
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label for="confirmpassword" class="col-sm-2 control-label">Confirm
							Password:</label>
						<div class="col-sm-4">
							<input type="password" id="confirmPassword" class="form-control"
								placeholder="Confirm Password *" />
						
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label for="password" class="col-sm-2 control-label">Contact
							Number:</label>
						<div class="col-sm-4">
							<form:input path="phone" id="phone" class="form-control"
								placeholder="ex: 919876543210" />
							<form:errors path="phone" cssClass="error" />	
						</div>
					</div>
				</div>
			</div>
			<div class="row">
			<div class="col-md-2"></div>
				<div class="col-md-10">
					<div class="form-group">
					    <div class="form-check">
								<input type="checkbox" name="termscheck" id="termscheck" class="form-check-input"
									checked="checked"> &nbsp; I have read and agree
								to <a href="https://spandana.org">Blood4Life.org</a> <a href="${baseDir}terms" target="_blank">Terms </a> and <a href="${baseDir}privacy" target="_blank">Privacy
									Policy.</a>
						</div>
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-sm-12">
					<div class="col-sm-4  text-center">
						<button type="submit" id="pro-signup-submit-button" tabindex="9"
							value="Submit" class="btn btn-default">Sign Up</button>
					</div>
				</div>
			</div>
			<div class="clearfix">
				<br /> <br />
				<br /> <br />
			</div>

		</form:form>
	</div>
</div>

<%@ include file="/WEB-INF/jsp/include/footer.jsp"%>