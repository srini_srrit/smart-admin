<!DOCTYPE html> 
<%@ include file="/WEB-INF/jsp/include/header.jsp"%> 
		<!-- Begin Main Wrapper -->
		<div class="container main-wrapper">
			<div class="main-content mag-content clearfix">
				<h3 class="small-title">
					<span>Contact Us</span> 
				</h3>
			 <br/> 
				<form:form method="post" name="contactusform" id="contactusform"
					modelAttribute="contact" action="contact-us">
						<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
					<form:errors path="*" cssClass="errorblock" element="div" />
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<form:input path="name" id="name" tabindex="1"
									class="form-control" placeholder="Name *" />
							</div>
						</div>
						</div>
						<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<form:input path="email" id="email" tabindex="2"
									class="form-control" placeholder="Email *" />
							</div>
						</div>
						</div>
						<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<form:input path="subject" id="subject" tabindex="3"
									class="form-control" placeholder="Subject *" />
							</div>
						</div>

					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<form:textarea path="message" name="message" cols="50" rows="6"
									tabindex="4" class="form-control" placeholder="Your message *" />
							</div>
						</div>
					</div>
				<%-- 	<div class="row">
						<div class="col-md-1">
							<div class="g-recaptcha" data-sitekey="6LeuazoUAAAAAKN2D0Us1Gswyg6hxnck-8tw9aMw"></div>
						</div>
					</div>
					
					<c:if test="${not empty recaptcha-error}">
								<div class="row">
									<div class="col-md-8 col-sm-10 col-xs-12">
										<div class="image-upload-validation-error">
											<p>${recaptcha-error}</p>
										</div>
									</div>
								</div>
					</c:if>
					<br/> --%>
					<div class="row">
						<div class="col-md-6">
							<button type="submit" id="submit-button"
								tabindex="5" value="Submit" class="btn btn-default">Submit</button>
						</div>
					</div> 
					<br/>
				</form:form>
				</div>
				</div>
	<!-- End Main -->
			<!--  FOOTER  -->
			<%@ include file="/WEB-INF/jsp/include/footer.jsp"%> 