  <!DOCTYPE html> 
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>
		<!-- Begin Main Wrapper -->
		<div class="container main-wrapper">
			<div class="main-content mag-content clearfix">
				<h3 class="small-title">
					<span>Reset Password </span> 
				</h3> 
	 	
			<c:choose>
			<c:when test="${not empty mesg}">  
				<div class="row"> 
					<div class="col-md-6">
						<div class="alert alert-danger">
							<p>${mesg}</p>
						</div>
					</div>
				</div>  
				<form method="post" name="resetpassword" id="resetpassword" action="reset-password" > 
				<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" /> 
					<div class="row">
							<div class="col-md-3">
								<div class="form-group">
								<label for="email">Email: </label>
									<input name="email" id="email" tabindex="1"
										class="form-control" placeholder="Email *" />
								</div>
							</div>
						</div> 
						
							<div class="row">
									<div class="col-md-12">
										<button type="submit" id="resetpassword-button"
											tabindex="5" value="Submit" class="btn btn-default">Reset Password</button> 
									</div>
								</div>
								<br/>
								<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									Don't have an account?<a href="${baseUrl}signup"> Sign Up </a>
								</div>
							</div>
						</div>
								<br />
				</form>
			</c:when>
			<c:otherwise> 
				<c:choose>
			 	<c:when test="${not empty email}">
			 	<form name="resetpassword">
					<div class="row"> 
							<div class="col-md-8">
								<div class="form-group">
						<p class="alert alert-success">If a valid email address has been provided, a reset link shall be sent to you. If you do not see the email in a few minutes, please check your spam filter for a message from <b>${email}</b>.</p>
						<br />
						<!-- <p>It may take a few minutes to receive your email. Be sure to
							check your spam folder if you can't find it.</p>
						<p>When you get it, please click through the link to change
							your password.</p> -->
						</div>
						</div> 
						<br /> 
					</form>
				</c:when>
				<c:otherwise>  
				<form method="post" name="resetpassword" id="resetpassword" action="reset-password" > 
				<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
						<p>Enter the email address associated with your account and click Reset password. <br/>We'll email you a link to reset your password.</p>				
					<div class="row"> 
							<div class="col-md-3">
								<div class="form-group">
								<label for="email">Email: </label>
									<input name="email" id="email" tabindex="1"
										class="form-control" placeholder="Email *" />
								</div>
							</div>
						</div> 
						
							<div class="row">
									<div class="col-md-12">
										<button type="submit" id="resetpassword-button"
											tabindex="5" value="Submit" class="btn btn-default">Reset Password</button>
									</div>
								</div>
								<br />
				</form>
				</c:otherwise>		
				</c:choose>	 
			</c:otherwise>
			</c:choose>
			</div>
			</div>
	<!-- End Main -->
			<!--  FOOTER  -->
			<%@ include file="/WEB-INF/jsp/include/footer.jsp"%> 