 <!DOCTYPE html> 
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>
		<!-- Begin Main Wrapper -->
		<div class="container main-wrapper"> 
			<div class="main-content mag-content clearfix">
			 <ul class="breadcrumb">
		           <li><a href="${baseUrl}">Home</a></li>  
		          	  <li>My Profile</a></li>     
	            </ul> 
				<h3 class="small-title">
					<span>Update Profile</span> 
				</h3>
			 <br/> 
			<form:form method="post" name="editProfile" id="editProfile"
					modelAttribute="provider" action="editProfile">
				<form:hidden path="providerId"/>	  
					<div class="row">  
						<div class="col-md-3">
								<div class="form-group">
								 <label for="email">Email:</label> 
									<form:input path="user.email" id="email" 
										class="form-control" placeholder="Email *" readonly="true" disabled="true"/> 
							</div>
						</div>
				 	</div> 
				<div class="row"> 
						<div class="col-md-3">
							<div class="form-group">
							<label for="firstName">First Name:</label>
								<form:input path="user.firstName" id="firstName"  
									class="form-control" placeholder="First Name *" />
							</div>
						</div>
					</div>
				<div class="row"> 
						<div class="col-md-3">
							<div class="form-group">
							<label for="lastName">Last Name:</label>
								<form:input path="user.lastName" id="lastName" 
									class="form-control" placeholder="Last Name *" />
							</div>
						</div>
					</div>		
					<label for="phone">Contact Phone:</label>
					<div class="row">
						<div class="col-md-1">
							<div class="form-group form-group-inline">
								<form:select class="form-control" path="countryCode"
									id="countryCode">
									<form:options items="${countryCode}" />
								</form:select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<form:input path="phone" id="phone"  
									class="form-control" placeholder="Contact Phone " />
							</div>
						</div>
					</div>
					 
					<div class="row"> 
						<div class="col-md-4">
							<div class="form-group">
							<label for="paymentId">PayPal Account: (** Event organizers only)</label>
								<form:input path="paymentId" id="paymentId"  
									class="form-control" placeholder="PayPal Account" /><br/>
							</div>
						</div>
					
					</div>	 
						 
					<%-- <div class="row">
						<div class="col-md-12">
							<div class="form-group"> 
							<div class="col-sm-10">
								 <p><input type="checkbox" name="contestcheck" id="contestcheck"> &nbsp;&nbsp;Yes, I would like participate Nriwala.com Sign up Contest <a href="${baseDir}promotion-terms" target="_blank">Promotion Terms</a></p>
							 
							 	</div>
							</div>
						</div>
					</div> --%>
					<div class="clear"><br/></div> 
				 	<div class="row">
							<div class="col-md-12"> 
							 		<button type="submit" id="submit-button"
										tabindex="5" value="Submit" class="btn btn-default">Update Profile</button>  
								</div>
							</div> 
							<br/><br/>
				</form:form> 
				</div>
				</div>
	<!-- End Main -->
			<!--  FOOTER  -->
			<%@ include file="/WEB-INF/jsp/include/footer.jsp"%> 