  <!DOCTYPE html> 
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>
		<!-- Begin Main Wrapper -->
		<div class="container main-wrapper">
			<div class="main-content mag-content clearfix">
				<h3 class="small-title">
					<span>Expired Password</span>
				</h3>
			 <br/> 
					<p>
					The link from your email has expired. <br />
				</p>
				 
						<div class="row">
								<div class="col-md-12">
									 <a href="${baseDir}resetPassword"><button type="submit" id="reset-password-submit-button"
										tabindex="3" value="Submit" class="btn btn-default">Reset your Password</button></a>
								</div>
							</div>
							<br />
							</div>
							</div>
	<!-- End Main -->
			<!--  FOOTER  -->
			<%@ include file="/WEB-INF/jsp/include/footer.jsp"%> 