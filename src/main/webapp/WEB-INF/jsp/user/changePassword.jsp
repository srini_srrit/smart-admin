 <!DOCTYPE html> 
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>
		<!-- Begin Main Wrapper -->
		<div class="container main-wrapper">
			<div class="main-content mag-content clearfix">
				<h3 class="small-title">
					<span>Reset Password</span>
				</h3>
			 <br/>  
			<form:form method="post" id="change-password-from" action="changePassword" modelAttribute="user">   
					 <form:hidden path="email" id="email" name="email"/>
					<p>
					Please enter your new password below. <br />
					<br />The password must be more than 6 and less than 30 characters long and cannot same as your email address.
				</p>
				<div class="row"> 
						<div class="col-md-3">
							<div class="form-group">
								<input type="password" name="password" id="password" tabindex="1"
									class="form-control" placeholder="Password *" />
							</div>
							
						</div>
						</div>
							<div class="row"> 
						<div class="col-md-3">
							<div class="form-group">
								<input type="password" name="confirmpassword" id="confirmpassword" tabindex="2"
									class="form-control" placeholder="Confirm Password *" />
							</div>
						</div>
					</div> 
					
						<div class="row">
								<div class="col-md-12">
									<button type="submit" id="reset-password-submit-button"
										tabindex="3" value="Submit" class="btn btn-default">Reset Password</button>
								</div>
							</div>
							<br />
				</form:form>
				</div></div>
	<!-- End Main -->
			<!--  FOOTER  -->
			<%@ include file="/WEB-INF/jsp/include/footer.jsp"%> 