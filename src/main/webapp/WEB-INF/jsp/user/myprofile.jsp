   <!DOCTYPE html> 
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>
		<!-- Begin Main Wrapper -->
		<div class="container main-wrapper"> 
		
		<div class="main-content mag-content clearfix">  
		 <ul class="breadcrumb">
		           <li><a href="${baseUrl}">Home</a></li>  
		          	  <li>My Profile</a></li>  
	            </ul> 
				<div class="col-md-12">   
					<h3 class="small-title">
					<span>My Profile</span> 
					</h3> 
				<div class="col-md-10"> 
						<p>First Name :  <c:out value="${profile.user.firstName}" /> </p>  
						<p>Last Name : <c:out value="${profile.user.lastName}" /> </p>   
						<p>Username: <b> <c:out value="${profile.user.username}" /></b></p>  
						<c:if test="${not empty profile.phone}"> 
							<p>Contact Phone:  <c:out value="${profile.countryCode}" /><c:out value="${profile.phone}" /> </p>   
						</c:if>
						
						<c:if test="${not empty profile.paymentId}"> 
						<p>** PayPal Account:  <c:out value="${profile.paymentId}"/></p>  
						</c:if>
						<br/><br/> 
						<a href="${baseUrl}editProfile?id=${profile.providerId}"><button name="submit" type="button" id="submit-button" value="Submit" class="btn btn-default">Edit Profile</button></a>
						<c:if test="${isSocialLogin ne true}">
						&nbsp;&nbsp; 
									<a href="/updatePassword?username=${userName}"><button type="button" id="change-password-button" class="btn btn-default">Update Password</button></a>
										</c:if>
									<div class="row"> 
			<br/><br/>
			</div>
		 	 	</div> 	 
				</div>
				</div>
				</div>
	<!-- End Main -->
			<!--  FOOTER  -->
			<%@ include file="/WEB-INF/jsp/include/footer.jsp"%> 