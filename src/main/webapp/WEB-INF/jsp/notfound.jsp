<!DOCTYPE html> 
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>
		<!-- Begin Main Wrapper -->
		<div class="container main-wrapper">
			<div class="main-content mag-content clearfix">
				<h3 class="small-title"></h3>
				<h4>${noResults}</h4>
				<div class="clearfix">
					<br />
				</div>
				<div class="clearfix">
					<br />
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<a href="${baseUrl}"><button name="home" type="button"
									id="submit-vm" tabindex="1" value="Home"
									class="btn btn-default">Back to Home</button></a>
						</div>
					</div>
					<%-- <div class="col-md-9"> 
							 <a href="${baseUrl}adsSignup"><button name="filter-submit-button" type="submit" id="filter-submit-button"
								tabindex="5" value="filter-submit-button" class="btn btn-default">Post Your Ad</button></a> 
					 	</div>   --%>
				</div>
				<div class="clearfix">
					<br />
				</div>
				<div class="clearfix">
					<br />
				</div>
			</div>
		</div>
 <%@ include file="/WEB-INF/jsp/include/footer.jsp"%> 