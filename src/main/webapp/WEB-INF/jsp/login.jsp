<!doctype html>
<html>
<%@ include file="/WEB-INF/jsp/include/includes.jsp"%>
<head>
<c:url var="baseUrl" value="/" />
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>NRI WALA | The local community source - Login Page</title>
<meta name="keywords" content="NRI WALA | The local community source">
<meta name="description" content="NRI WALA | The local community source">
<meta class="viewport" name="viewport"
	content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>
<body>
	<div id="main" class="header-style1">
		<header class="header-wrapper clearfix">
			<div class="header" id="header"> 
			</div>
		</header>
		<!-- Begin Main Wrapper -->
		<div class="container main-wrapper">
			<div class="main-content mag-content clearfix">
				<h3 class="small-title">
					<span>Sign In</span>
				</h3>
				<br />
				<div class="row">
					<div class="col-md-6">
						<c:if test="${not empty error}">
							<div class="row">
								<div class="col-md-10 col-sm-12 col-xs-12">
									<div class="alert alert-danger">
										<p>${error}</p>
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${param.logout != null}">
							<div class="row">
								<div class="col-md-10 col-sm-12">
									<div class="alert alert-success">
										<p>You have been logged out successfully.</p>
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${resetMsg != null}">
							<div class="row">
								<div class="col-md-10 col-sm-12">
									<div class="alert alert-success">
										<p>${resetMsg}</p>
									</div>
								</div>
							</div>
						</c:if>
						<form action="${baseUrl}login" method="post"
							id="login-submit-form">
							<div class="row">
								<div class="col-md-6 col-sm-5 col-xs-10">
									<div class="form-group">
										<label for="email">Email:</label> <input type="text"
											name="email" tabindex="1" class="form-control"
											placeholder="Email *" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-sm-5 col-xs-10">
									<div class="form-group">
										<label for="password" class="">Password:</label> <input
											type="password" name="password" tabindex="3"
											class="form-control" placeholder="Password *" />
									</div>
									<a href="${baseDir}reset-password">Forgot Password</a>
								</div>
							</div>
							<!-- <div class="row">
								<div class="col-md-4"> 
			                 		 <label>
			                  		  <input type="checkbox" name="remember-me" checked="checked"/>&nbsp;Remember Me      
			               		 </label>  
			               		 
			                </div>
			                </div> -->
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" /> <br />
							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn btn-default"
										id="login-submit-button">Sign In</button>
								</div>
							</div>
						</form>
						<br />

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									Don't have an account?<a href="${baseUrl}signup"> Sign Up </a>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-5">
								<div class="form-group">
									Sign in with <a href="facebook/connect"><img
										src="${s3BucketPath}static/nw/sm/1.png" height="33px"
										width="33px" /></a> <a href="google/connect"> <img
										src="${s3BucketPath}static/nw/sm/2.jpg" height="35px"
										width="35px" /></a>
								</div>
								<br /> <br />
							</div>
						</div>
						<div class="clearfix">
							<br />
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Main Banner -->
	<!-- End Main -->
	<!--  FOOTER  -->
	<%@ include file="/WEB-INF/jsp/include/footer.jsp"%>
</body>
</html>