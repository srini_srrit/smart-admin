<!DOCTYPE html> 
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>
		<!-- Begin Main Wrapper -->
		<div class="container main-wrapper">
			<div class="main-content mag-content clearfix"> 
			<c:choose>
			<c:when test="${mesg == null}">
				<h5>System error has occurred, and your request did not go through properly. Please contact us immediately at 
				<a href="mailto:contact@nriwala.com">contact@nriwala.com</a>, so that we may quickly rectify the situation to your complete satisfaction.<h5> 
			</c:when>
			<c:otherwise>
				<h5><c:out value="${mesg}"/></h5>
			</c:otherwise>
			</c:choose> 
			<h5>We apologize for any inconvenience.</h5><br /> <br />  
 				<div class="clearfix"><br/></div>
				<div class="row">
					<div class="col-md-3"> 
						<div class="form-group">
										<a href="${baseUrl}"><button name="home" type="button" id="submit-vm"
									tabindex="1" value="Home" class="btn btn-default">Back to Home</button></a>
									</div>
						</div>   
				</div>
		</div>
		</div>
		 <%@ include file="/WEB-INF/jsp/include/footer.jsp"%> 