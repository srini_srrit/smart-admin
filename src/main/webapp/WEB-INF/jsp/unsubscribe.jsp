<!DOCTYPE html> 
<%@ include file="/WEB-INF/jsp/include/header.jsp"%> 
		<!-- Begin Main Wrapper -->
		<div class="container main-wrapper">
			<div class="main-content mag-content clearfix">
				<h3 class="small-title">
					<span>Unsubscribe</span> 
				</h3>
			 <br/> 
				<form:form name="unsubscribeForm" id="unsubscribeForm" action="unsubscribe" modelAttribute="subscribe"> 
					<div class="row">
						<div class="col-md-8">
							 <span>This will stop all emails from Blood4Life.org.<br/><br/></span>
						</div>
					</div> 
					<div class="row"> 
							<div class="col-md-3">
								<div class="form-group">
								<label for="email">Email Address *: </label>
									<form:input path="email" id="email" tabindex="1" class="form-control" placeholder="Email Address *" /> 
										
								</div>
							</div>
						</div> 
						<div class="row">
							<div class="col-md-6">
								*If this isn't your email address you haven't been added to any mailing lists and there is no need to unsubscribe.<br/><br/><br/>
							</div>
						</div>
							<div class="row">
									<div class="col-md-12">
										<button type="submit" id="resetpassword-button"
											tabindex="2" value="Submit" class="btn btn-default">Unsubscribe</button>
									</div>
								</div>
								<br />
				</form:form>
				</div>
				</div>
	<!-- End Main -->
			<!--  FOOTER  -->
			<%@ include file="/WEB-INF/jsp/include/footer.jsp"%> 