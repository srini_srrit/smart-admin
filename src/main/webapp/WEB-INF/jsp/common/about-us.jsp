<!DOCTYPE html> 
<%@ include file="/WEB-INF/jsp/include/header.jsp"%>
		<!-- Begin Main Wrapper -->
		<div class="container main-wrapper">
			<div class="main-content mag-content clearfix">
				<h3 class="small-title">
					<span>AccessDenied page</span>
				</h3>
			 <br/>
			   Dear <strong>${user}</strong>, You are not authorized to access this page
    			<a href="<c:url value="/logout" />">Logout</a>
			</div>
			</div>
			<!--  FOOTER  -->
			<%@ include file="/WEB-INF/jsp/include/footer.jsp"%>