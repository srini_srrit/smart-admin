<!DOCTYPE html>  
<%@ include file="/WEB-INF/jsp/include/header.jsp"%> 

     <!--  HOME 2 BANNER BLOCK  -->

        <section class="section-banner" data-bg_img="${basePath}images/wb1.jpg" data-bg_color="#111111" data-bg_opacity="0.1" >

            <div class="container wow fadeInUp">

                <div class="row">
                    <div class="col-md-12 col-sm-12 text-center">
                        <div class="banner-content">
                            <h2>
                                Give Blood, Give Life.
                            </h2>					
                            <h3>Blood is the most precious gift that anyone can give to another person.<br>
                               THE GIFT OF LIFE.
                            </h3>

                            <a href="${baseUrl}signup" class="btn btn-theme margin-top-32">DONATE BLOOD</a>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <a href="${baseUrl}donors-search" class="btn btn-theme margin-top-32">FIND DONOR</a>
                        </div>
                    </div> <!-- end .col-md-12  -->
                </div>

            </div>

        </section>

        <!--  SECTION DONATION PROCESS -->

        <section class="section-content-block section-process">

            <div class="container">

                <div class="row hidden">

                    <div class="col-md-12 col-sm-12 text-center">
                        <h2 class="section-heading"><span>Donation</span> Process</h2>
                        <p class="section-subheading">The donation process from the time you arrive center until the time you leave</p>
                    </div> <!-- end .col-sm-10  -->                    

                </div> <!--  end .row  -->
                
                <div class="row section-heading-wrapper">

                    <div class="col-md-12 col-sm-12 text-center no-img-separator">
                        <h2>DONATION PROCESS</h2>
                        <span class="heading-separator heading-separator-horizontal"></span> 
                    </div> <!-- end .col-sm-10  -->                      


                </div> <!-- end .row  -->

                <div class="row wow fadeInUp">

                    <div class="col-lg-4 col-md-offset-0 col-md-4 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12">
				
                        <div class="process-layout">
	
                            <figure class="process-img">
								<h5>One time registration</h5>
                                <img src="${basePath}images/reg1.jpg" alt="service" />
                                <div class="step">
                                    <h3>1</h3>
                                </div>
                            </figure> <!-- end .cause-img  -->

                            <article class="process-info">
                                <h2>Registration</h2>   
                                <p>You need to complete a very simple registration form to start donating blood.</p>
                            </article>
							<a href="${baseUrl}signup" class="btn btn-theme margin-top-32">Register Now</a>
                        </div> <!--  end .process-layout -->

                    </div> <!--  end .col-lg-3 -->


					<h5>from the time you arrive at the center until the time you leave</h5>
                    <div class="col-lg-4 col-md-offset-0 col-md-4 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12">
				
                        <div class="process-layout">
					
                            <figure class="process-img">
                            	
                                <img src="${basePath}images/reg2.jpg" alt="process" />
                                <div class="step">
                                    <h3>2</h3>
                                </div>
                            </figure> <!-- end .cau<h5 class="step">1</h5>se-img  -->

                            <article class="process-info">                                   
                                <h2>Screening</h2>
                                <p>A drop of blood sample will be taken from your finger to perform a quick test to ensure that your blood is safe for donations.</p>
                            </article>

                        </div> <!--  end .process-layout -->

                    </div> <!--  end .col-lg-3 -->


                    <div class="col-lg-4 col-md-offset-0 col-md-4 col-md-offset-0 col-sm-10 col-sm-offset-1 col-xs-12">

                        <div class="process-layout">

                            <figure class="process-img">
                                <img src="${basePath}images/reg3.jpg" alt="process" />
                                <div class="step">
                                    <h3>3</h3>
                                </div>
                            </figure> <!-- end .cause-img  -->

                            <article class="process-info">
                                <h2>Donation</h2>
                                <p>After successful screening , you will be directed to a donor bed for drawing blood by certified medical staff. The entire process might take approximately 5 to 30 minutes.</p>
                            </article>

                        </div> <!--  end .process-layout -->

                    </div> <!--  end .col-lg-3 -->

                </div> <!--  end .row --> 

            </div> <!--  end .container  -->

        </section> <!--  end .section-process -->
        
        
        
        <!--  SECTION CAMPAIGNS   -->

        <section class="section-content-block section-secondary-bg" >

            <div class="container">

                <div class="row section-heading-wrapper">

                    <div class="col-md-12 col-sm-12 text-left no-img-separator">
                        <h2>OUR CAMPAIGNS</h2>
                        <span class="heading-separator heading-separator-horizontal"></span>
                        <h4>Encourage new donors to join and continue to give blood. We have total sixty thousands donor centers and visit thousands of other venues on various occasions. </h4>
                    </div> <!-- end .col-sm-12  -->                       

                </div> <!-- end .row  --> 
                
                

                <div class="row">
                	<h3>Interested to conduct a campaign in your city?</h3>
                    <div class="col-md-12 col-sm-12 text-center">
                        <a class="btn btn-theme margin-top-48" href="${baseUrl}contact-us">Contact Us</a>
                    </div>
                </div>

            </div> <!--  end .container  --> 

        </section> 
        
        <section class="cta-section-2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <h2>We have been helping people since 13 years</h2>
                        <p>
                            There is a constant need for regular blood supply because blood can be stored for only a limited time before use. Regular blood donations by a sufficient number of healthy people are needed to ensure that safe blood will be available whenever and wherever it is needed.                            
                        </p>
                        <a class="btn btn-theme btn-theme-invert margin-top-24 wow bounceIn" href="${baseUrl}signup">DONOTE BLOOD</a>
                    </div> <!--  end .col-md-8  -->
                </div> <!--  end .row  -->
            </div>
        </section>  
       
	     
 <%@ include file="/WEB-INF/jsp/include/footer.jsp"%> 