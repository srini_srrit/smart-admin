 jQuery(function ($) {
	
	var dataSet = [
		[ "Fiona Green", "Chief Operating Officer (COO)", "San Francisco" ]
	
	];
	$(document).ready(function() {
		$('#example').DataTable( {
	        data: dataSet,
	        columns: [
	            { title: "Name" },
	            { title: "Phone Number" },
	            { title: "Address" }
	        ]
	    } );
	} );
	
	new google.maps.event.addDomListener(window, 'load', initMapData);
	
	var autocomplete, map, marker;
	var lat, lag, searchReduis;

	function initMapData() {
		initMap();
		initAutocomplete();
	} 
	 
	// Initialize and add the map
	function initMap() {
	  // The location of hyd 
	  var hyd = {lat: 17.3850, lng: 78.4867};
	  // The map, centered at hyd
	  map = new google.maps.Map(
	      document.getElementById('map'), {zoom: 4, center: hyd});
	  // The marker, positioned at Uluru
	  marker = new google.maps.Marker({position: hyd, map: map});
	}
	
	
	function initAutocomplete() {
		// Create the autocomplete object, restricting the search to geographical
		// location types.

		var input = document.getElementById('pac-input');
		var options = {
			componentRestrictions : {
				country : 'in'
			}
		//India only
		};
		autocomplete = new google.maps.places.Autocomplete(input, options);

		// When the user selects an address from the dropdown, populate the address
		// fields in the form.
		autocomplete.addListener('place_changed', fillInAddress);
	}
	
	function fillInAddress() {
		// Get the place details from the autocomplete object.
		var place = autocomplete.getPlace();
		if (place.geometry.viewport) {
            lat = (place.geometry.viewport.f.b+place.geometry.viewport.f.f)/2;
            lag = (place.geometry.viewport.b.b+place.geometry.viewport.b.f)/2
          } else {
            bounds.extend(place.geometry.location);
            lat = place.geometry.location.lat();
            lag = place.geometry.location.lag();
        }
		
		var location = {lat: lat, lng: lag};
		map = new google.maps.Map(document.getElementById('map'), {zoom: 15, center: location });
		  // The marker, positioned at selected location
		//var marker = new google.maps.Marker({position: new google.maps.LatLng(location), map: map});
		  
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(lat, lag),
	
			map: map
		});
	}
	
	function addThumnailContent(donor, n ){
		
		var thumbnail = "<div class='row'>";
		thumbnail += "<div class='col-md-12' onclick=''><div class='thumbnail'> ";
		thumbnail += "<div class='caption'><h4>"+donor.donorName+"</h4><p>"+donor.address.locality+"</p>";
		/* thumbnail += "<p> Distance : "+donor.distance.toFixed(2)+" Km </p>"; */
		thumbnail += "<p><a href='#' class='btn btn-primary' role='button'>Contact</a></p> ";
		thumbnail += "</div></div></div></div>"
		
		jQuery('#thumbnaiList').append(thumbnail);
		
	};
	
	function getDonorDetailsByInputs() {
		var e = document.getElementById("radius");
		searchReduis = e.options[e.selectedIndex].value;

		if (lat == null || lag == null || searchReduis == null) {
			return;
		}

		console.log(lat + "-->" + lag);
		 var jsonInput = {
			"range" : searchReduis,
			"lattitude" : lat.toFixed(4),
			"longitude" : lag.toFixed(4)
		}; 
		
		/* var jsonInput = {
				"lattitude" : 17.55,
				"longitude" : 76.55,
				"range" : 15
			}; */

		jQuery.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json'
			},
			url : "/search/donors-search",
			method : "POST",
			dataType : 'json',
			data : JSON.stringify(jsonInput)
		}).done(function(data) {
			
			var count = data.count;
			var donors = data.results;
			
			
			map = new google.maps.Map(document.getElementById('map'), {
	    		zoom: 10,
	    		center: new google.maps.LatLng(lat, lag),
	    		mapTypeId: google.maps.MapTypeId.ROADMAP
	    	});

	    	marker = new google.maps.Marker({
				position: new google.maps.LatLng(lat, lag),
		
				map: map
			});
	    	
	    	var infoWindow = new google.maps.InfoWindow();
	    	
	    	for (var i = 0, length = count; i < count; i++) {
	    		
	    		 var data = donors[i];
	    		 if(data.address != null){
	    			 
	    			 addThumnailContent(data, i);
	    			 
	    			 var latLng = new google.maps.LatLng(data.address.lattitude, data.address.longitude); 

		    		  // Creating a marker and putting it on the map
		    		  var marker = new google.maps.Marker({
		    		    position: latLng,
		    		    map: map,
		    		    title: donors[i].donorName
		    		  }); 
		    		  
		    		// Creating a closure to retain the correct data 
		    		//Note how I pass the current data in the loop into the closure (marker, data)
		    		(function(marker, data) {

		    			console.log(data.donorName);
		    		  // Attaching a click event to the current marker
		    		  google.maps.event.addListener(marker, "click", function(e) {
		    		    infoWindow.setContent('<div><strong>' + data.donorName + '</strong><br><div><strong>'+data.address.locality+'</strong><br><div>' );
		    		    infoWindow.open(map, marker);
		    		  });

		    		})(marker, data);
	    		 }
	    	}
			
		}).fail(function(xhr) {
			console.log('error', xhr);
		});
	}
 });