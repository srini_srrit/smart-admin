$(document).ready( function(){
	var regEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; 
	// pre-populate sub category based on category selection.

	 var state =  $('select[name="state"] option:selected').val(); 
	 $('#stateSelected').val(state);   

	$("#sb-search-submit-text").click(function() { 
		var searchStr= $('#searchString').val();   
		var flag = false;
		if (searchStr != null && searchStr != '' && searchStr != undefined) { 
		 	$('#mainSearchForm').attr('action', 'search-results');
			$('#mainSearchForm').submit();
			flag = true; 
		} 
		return flag;
	});
	/* $("#contactPhone").attr({ maxLength : 15 }); */
	$('#phone').attr('placeholder',"(000) 000-0000")
	.keyup(function(e) {
	    if(e.keyCode!=9&&e.keyCode!=8 && e.keyCode!=37 && e.keyCode!=39){
	   	 this.value = this.value.replace(/[^0-9]/g,'')
	   	 			.replace(/(\d{0,3})(\d{0,3})(\d{0,4})/,'($1) $2-$3')
	   	 			.replace(/(\(\))?( -$)/,'');
	    }
	});

	  $('#postAdsZipcode').keyup(function(e) {
			if(e.keyCode!=9&&e.keyCode!=8 && e.keyCode!=37 && e.keyCode!=39){
				this.value = this.value.replace(/[^0-9]/g,'');
			}
		});
	
	$('#login-submit-form').formValidation({
		framework: 'bootstrap', 
		fields: { 
			email: {
				validators: {
					notEmpty: {
						message: 'The email address is required'
					},
					regexp: {
	   		             regexp: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
	   		             message: 'The input is not a valid email address'
					} 
				}
			},
			password: {
				validators: {
					notEmpty: {
						message: 'The password is required'
					},
					stringLength: {
						min: 6,
						max: 30,
						message: 'Password must be between 6 - 30 characters.'
					},
					different: {
						field: 'username',
						message: 'The password cannot be the same as username'
					}
				}
			} 
		}
	});    

	$('#userSignupForm').formValidation({ 
		framework: 'bootstrap', 
		fields: { 
			firstName: { 
				selector: '#firstName',
				validators: {
					notEmpty: {
						message: 'The first name is required and cannot be empty'
					},
					 regexp: {
    		             regexp: /[a-zA-Z]{1,10}/,
    		             message: 'First name must be alpha numerics'
    		        } 
				}
			}, 
			lastName: { 
				selector: '#lastName',
				validators: {
					notEmpty: {
						message: 'The last name is required and cannot be empty'
					},
					 regexp: {
    		             regexp: /[a-zA-Z]{1,10}/,
    		             message: 'Last name must be alpha numerics'
    		        } 
				}
			},  
			email: {
				selector: '#email',
				validators: {
					notEmpty: {
						message: 'The email is required and cannot be empty'
					},
					 regexp: {
    		             regexp: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    		             message: 'The input is not a valid email address'
    		        } 
				}
			},
			password: {
				selector: '#password',
				validators: {
					notEmpty: {
						message: 'The password is required and cannot be empty'
					},
					stringLength: {
						min: 6,
						max: 30,
						message: 'The password must be more than 6 and less than 30 characters long.'
					} 
				}
			},
			confirmPassword: { 
				selector: '#confirmPassword',
				validators: {
					notEmpty: {
						message: 'The confirm password is required and cannot be empty'
					},
					identical: {
						field: 'password',
						message: 'The password and its confirm password are not the same'
					} 
				}
			}, 
			/*contactPhone: {  
	         	 selector: '#contactPhone', 
	         	validators: {
	         		 notEmpty: {
	                     message: 'The contact phone is required and cannot be empty'
	                 }, 
	                 stringLength: {
							min: 14,
							max: 14,
							message: 'The phone number is not valid'
						}
	         	}
	         },   */
			'termscheck': { 
				validators: {
					notEmpty: {
						message: 'You have to accept the terms and policies'
					}
				}
			},
		} 

	}); 
	
	$('#editProfile').formValidation({ 
		framework: 'bootstrap', 
		fields: { 
			firstName: { 
				selector: '#firstName',
				validators: {
					notEmpty: {
						message: 'The first name is required and cannot be empty'
					} 
				}
			}, 
			lastName: { 
				selector: '#lastName',
				validators: {
					notEmpty: {
						message: 'The last name is required and cannot be empty'
					}  
				}
			} 
		} 

	});